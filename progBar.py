class Progbar():
    def __init__(self, works, now=-1, bar_len=30, icon='#', message='Start.\n'):
        self.works    = works
        self.now      = now
        self.bar_len  = bar_len
        self.finish   = False
        self.icon     = icon
        self.icon_num = 0
        self.blank_num = bar_len
        self.prog_par = int(self.now/self.works) if self.now!=-1 else 0
        self.par_prev = -1
        self.bar_update = True
        self._progStart(message)

    def _progStart(self, message):
        print(message, end='')
        self._writeBar()

    def setIconNum(self):
        self.par_prev = self.prog_par
        self.prog_par = int(self.now/self.works*100)
        self.icon_num = int(self.now/self.works * self.bar_len)
        self.blank_num = self.bar_len - self.icon_num
        self.bar_update = self.prog_par != self.par_prev

    def progress(self, num, add_mode):
        self.now = self.now+num if add_mode else num

    def view(self, num=1, add_mode=True, message=None):
        self.progress(num, add_mode)
        self.setIconNum()
        if self.bar_update:
            self._writeBar()
        
        if type(message) == str:
            print('\n'+message, end='')

    def _writeBar(self):
        print('\r['+ self.icon*self.icon_num + ' '*self.blank_num +'] {}%'.format(self.prog_par), end='')

    def progFinish(self, message='Finish.\n'):
        self.view(self.works, False)
        print('\n'+message, end='')


if __name__ == "__main__":

    tasks = [i for i in range(10000)]
    p_bar = Progbar(len(tasks), message='Start Encoding.')
    p_bar2 = Progbar(len(tasks))

    for i, _ in enumerate(tasks):
        p_bar.view()
        p_bar2.view(i, False)

    p_bar.progFinish('Finish Encoding.')
    p_bar2.progFinish()