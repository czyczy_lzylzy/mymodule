import sys

class Command():
    
    def __init__(self, argv, option=dict()) -> None:
        self.argv = argv

        self.option = {
            '-H': self.cmdHelp
        }
        self.option.update(option)
    
    def setArgRule(self, arg_num:int, arg_rules:list, arg_exprain_list:list):
        self.arg_num = arg_num
        self.arg_rules = arg_rules
        self.run_text = '\npython '+self.argv[0]+' [OPTIONS]'
        self.arg_exprain_text = ''
        for i, arg_exprain in enumerate(arg_exprain_list):
            self.run_text += ' [{}]'.format(i)
            self.arg_exprain_text += '[{}] : {}\n'.format(i, arg_exprain)

    def getArg(self):
        str_arg_list = []
        option_list = []

        for i in self.argv[1:]:
            if '-' not in i:
                str_arg_list.append(i)
            else:
                if i == '-H':
                    self.cmdHelp()
                else:
                    option_list.append(i)

        self.checkArg(str_arg_list)
        self.checkOption(option_list)

        ## 引数の取得
        arg_list =[]
        for i, arg in enumerate(str_arg_list):
            try:
                arg_list.append(self.arg_rules[i](arg))
            except:
                raise ArgmentError('{}番目の引数の型に誤りがあります(0から)'.format(i))
        
        return arg_list

    def runOption(self, op:str, arg=[]):
        if self.isSetOption(op):
            return self.option[op](*arg)

    def checkArg(self, arg_list) -> None:
        if len(arg_list) != self.arg_num:
            raise ArgmentError('与える引数は{}つです。{}つ与えられています'.format(self.arg_num, len(arg_list)))
        

    def checkOption(self, option_list) -> None:
        cmd_set = set(self.option.keys())
        inp_cmd_set = set(option_list)

        if not (inp_cmd_set <= cmd_set):
            raise OptionError('"{}"はオプションにありません\n"-H"を指定してオプションを確認してください', )


    def cmdHelp(self) -> None:
        """コマンドライン引数のヘルプを表示"""
        print(self.run_text)
        print(self.arg_exprain_text)
        for key in self.option.keys():
            print('{} : {}'.format(key, self.option[key].__doc__))
        
        sys.exit(0)

    def addOption(self, cmd, func) -> None:
        self.option[cmd] = func

    def isSetOption(self, op:str) -> bool:
        return op in self.argv

class ArgmentError(Exception):
    """コマンドライン引数が誤った値である"""
    def __init__(self, *args: object) -> None:
        super().__init__(*args)

    def __str__(self) -> str:
        return super().__str__()

class OptionError(Exception):
    """オプションに無いものを指定している"""
    def __init__(self, *args: object) -> None:
        super().__init__(*args)

    def __str__(self) -> str:
        return super().__str__()


class Progbar():

    def __init__(self, works, now=0, bar_len=30, icon='#', message='Start.'):
        self.works    = works
        self.now      = now
        self.bar_len  = bar_len
        self.finish   = False
        self.icon     = icon
        self.icon_num = 0
        self.blank_num = bar_len
        self.prog_par = int(self.now/self.works)
        self.par_prev = -1
        self.bar_update = True
        print(message)

    def setIconNum(self):
        self.par_prev = self.prog_par
        self.prog_par = int(self.now/self.works*100)
        self.icon_num = int(self.now/self.works * self.bar_len) + 1
        self.blank_num = self.bar_len - self.icon_num
        self.bar_update = self.prog_par != self.par_prev

    def progress(self, num, add_mode):
        self.now = self.now+num if add_mode else num

    def view(self, num, add_mode=False):
        self.progress(num, add_mode)
        self.setIconNum()
        if self.bar_update:
            print('\r['+ self.icon*self.icon_num + ' '*self.blank_num +']', end='')
            print(' '+str(self.prog_par)+'%  ', end='')

    def progFinish(self, message='Finish.'):
        print('\n'+message)